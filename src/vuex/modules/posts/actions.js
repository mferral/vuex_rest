import axios from 'axios';

export const actions = {
  LOAD_POST_LIST: function ({ commit }) {
    axios.get('http://localhost:8000/posts/').then((response) => {
      commit('SET_PROJECT_LIST', { list: response.data })
    }, (err) => {
      console.log(err)
    })
  },
  ADD_POST: function ({ commit }, post) {

   axios.post('http://localhost:8000/posts/',post).then((response) => {
      commit('SET_POST', response.data )
    }, (err) => {
      console.log(err)
    })
  },
  DELETE_POST: function ({ commit }, item) {

   axios.delete(item.url).then((response) => {
      commit('REMOVE_POST', item )
    }, (err) => {
      console.log(err)
    })
  }
}
