import Vue from 'vue'
import App from './App.vue'
import Vuetify from 'vuetify'
import {store} from './vuex/store';
Vue.use(Vuetify)

new Vue({
  el: '#app',
  store,
  render: h => h(App)
})

<!-- https://medium.com/wdstack/vue-vuex-getting-started-f78c03d9f65 -->
<!-- https://medium.com/front-end-hacking/persisting-user-authentication-with-vuex-in-vue-b1514d5d3278 -->
