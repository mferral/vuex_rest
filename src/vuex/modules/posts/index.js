import { mutations } from './mutations'
import { actions } from './actions'
const posts = [];

export default {
    state: {
        posts:posts
    },
    mutations,
    actions,
};
