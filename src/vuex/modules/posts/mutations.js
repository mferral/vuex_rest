export const mutations = {
  SET_PROJECT_LIST: (state, { list }) => {
    state.posts = list;
  },
  SET_POST: (state, post) => {
    //state.posts.unshift(post);
    state.posts.push(post);
  },
  REMOVE_POST: (state, value) => {
    state.posts= state.posts.filter(item => item !== value);
  },
  SET_POST: (state, value) => {
    state.posts.update(value,{done:true})
  }
};
