import Vue from 'vue';
import Vuex from 'vuex';
import post from './modules/posts';
Vue.use(Vuex);

export const store = new Vuex.Store({
   state: {
       cantidad: 0,
   },
   modules: {
       post
   }
});
